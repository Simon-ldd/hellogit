# README

Git & Git flow practice

# 1. 创建HelloGit

``` 
git init            初始化git仓库
git flow init       初始化gitflow
```

# 2. 远程仓库

```
git remote add <shortname> <url>    添加远程仓库
git push <remote-name> <branch-name>    推送到远程仓库
```

# 3. 忽略文件

```
git config --local core.excludesfile /Users/simon/Work/Domob/practice/HelloGit/.gitignore   配置忽略.gitignore中的文件
```

# 4. 撤销工作目录下的修改

```
git checkout -- <file>
```

testing